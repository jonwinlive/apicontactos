<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Directorio;
use App\Http\Requests\CreateDirectorioRequest;
use App\Http\Requests\UpdateDirectorioRequest;

class DirectorioController extends Controller
{

    //GET, list  record
    public function index( Request $request )
    {
        //collect all the parameters of the request, $request->all();
        //check for search
        if ( $request->has('search') ) {
            $directories = Directorio::where('nombre','like','%'.$request->search.'%')
            ->orWhere('telefono',$request->search)
            ->get();
        } else {
            $directories = Directorio::all();
        }
        return $directories;
    }
    //POST, add record
    public function store(CreateDirectorioRequest $request)
    {
        $input = $request->all();
        if ( $request->isMethod('post') ) {
            //$request->exists('telefono')
            $name = $request->input('nombre');
            if ( isset($name) && !empty($name) ) {
                if ( $request->has('foto') ) 
                    $input['foto'] = $this->uploadFile( $request->foto );
                Directorio::create($input);
                return response()->json([
                    'response' => true,
                    'message' => 'Successful operation' 
                ], 200 );
            } else {
                return response()->json([
                    'response'=> false,
                    'message' => 'You need to send the data'
                ], 200 );
            }
        } else {
            return response()->json([
                'response'=> false,
                'message' => 'Upss..'
            ], 400 );
        }
    }

    //GET, return a record
    public function show(  $directory )
    {
        return  response()->json( Directorio::find($directory) ) ; 
    }
    //PUT, update record
    public function update(UpdateDirectorioRequest $request,  $directory )
    {
        $input  = $request->all();
        if ( $request->isMethod('PUT') ) {
            //$record  = Directorio::find( $directory );
            //$record->nombre = $input['nombre'];
            //$record->save();
            if ( $request->has('foto') ) 
                $input['foto'] = $this->uploadFile( $request->foto );

            Directorio::where('id' , $directory )->update([
                'nombre' => $input['nombre'],
                'direccion' => $input['direccion'],
                'telefono' => $input['telefono'],
                'foto' => isset($input['foto']) ?  $input['foto'] : null
            ]);
            return response()->json([
                'response'=> Directorio::find($directory),
                'message' => 'Successful operation' 
            ], 200 );
        } else {
            return response()->json([
                'response' => false,
                'message'=> 'Upss...'
            ], 400 );
        }
    }
    //DELETE, delete record
    public function destroy($id)
    {
        Directorio::destroy( $id );
        return response()->json([
            'response' => true,
            'message' => 'Successful operation' 
        ], 200 );
    }
    private function uploadFile( $file ){
        $name = time().".".$file->getClientOriginalExtension();
        $file->move( public_path('photos'), $name );
        return $name;
    }
}
