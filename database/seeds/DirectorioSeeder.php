<?php

use Illuminate\Database\Seeder;

class DirectorioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directorios')->insert([
            [
                'nombre'=> 'sarah',
                'direccion'=>'colonia vellan',
                'telefono'=> 98763456,
                'foto'=> null
            ],
            [
                'nombre'=> 'imara',
                'direccion'=>'colonia vellan',
                'telefono'=> 28763256,
                'foto'=> null
            ]
        ]);
    }
}
