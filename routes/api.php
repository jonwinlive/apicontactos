<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// POST, add record
Route::post('users', 'UserController@store');
// POST login
Route::post('login', 'UserController@login');

Route::group(['middleware'=>'auth:api'], function(){
    //GET: list  record,
    //test: api/directorios , api/directorios?search=myquery
    Route::ApiResource('directorios','DirectorioController');
    //POST, logout
    Route::post('logout', 'UserController@logout');
});

